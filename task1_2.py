from __future__ import division, print_function, absolute_import

import tensorflow as tf
import numpy as np

# Training Parameters
learning_rate = 0.001
num_steps = 20000

# Network Parameters
num_input = 1280
#num_input = 784 # MNIST data input (img shape: 28*28)
num_classes = 2 # MNIST total classes (0-9 digits)
dropout = 0.4 # Dropout, probability to keep units


# Create the neural network
def conv_net(x_dict, n_classes, dropout, reuse, is_training):
    # Define a scope for reusing the variables
    with tf.variable_scope('ConvNet', reuse=reuse):
        # TF Estimator input is a dict, in case of multiple inputs
        x = x_dict['sample']

        # MNIST data input is a 1-D vector of 784 features (28*28 pixels)
        # Reshape to match picture format [Height x Width x Channel]
        # Tensor input become 4-D: [Batch Size, Width, Channel]
        x = tf.reshape(x, shape=[-1, num_input, 1])

        # Convolution Layer with 32 filters and a kernel size of 5
        conv1 = tf.layers.conv1d(x, 32, 5, activation=tf.nn.relu, padding='same')
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        pool1 = tf.layers.max_pooling1d(conv1, 2, 2)

        # Convolution Layer with 64 filters and a kernel size of 5
        conv2 = tf.layers.conv1d(pool1, 64, 5, activation=tf.nn.relu, padding='same')
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        conv2 = tf.layers.max_pooling1d(conv2, 2, 2)

        # Convolution Layer with 64 filters and a kernel size of 3
        #conv3 = tf.layers.conv1d(conv2, 64, 3, activation=tf.nn.relu)
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        # conv3 = tf.layers.max_pooling1d(conv3, 2, 2)

        conv2_flat = tf.reshape(conv2, [-1, 320 * 64])
        dense = tf.layers.dense(inputs=conv2_flat, units=1024, activation=tf.nn.relu)
        dropout = tf.layers.dropout(
            inputs=dense, rate=dropout, training=is_training == tf.estimator.ModeKeys.TRAIN)

        # Output layer, class prediction
        out = tf.layers.dense(dropout, n_classes)

    return out


# Define the model function (following TF Estimator Template)
def model_fn(features, labels, mode):
    # Build the neural network
    # Because Dropout have different behavior at training and prediction time, we
    # need to create 2 distinct computation graphs that still share the same weights.
    logits_train = conv_net(features, num_classes, dropout, reuse=False,
                            is_training=True)
    logits_test = conv_net(features, num_classes, dropout, reuse=True,
                           is_training=False)

    # Predictions
    pred_classes = tf.argmax(logits_test, axis=1)
    pred_probas = tf.nn.softmax(logits_test)

    # If prediction mode, early return
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=pred_classes)

        # Define loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logits_train, labels=tf.cast(labels, dtype=tf.int32)))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op,
                                  global_step=tf.train.get_global_step())

    # Evaluate the accuracy of the model
    acc_op = tf.metrics.accuracy(labels=labels, predictions=pred_classes)

    # TF Estimators requires to return a EstimatorSpec, that specify
    # the different ops for training, evaluating, ...
    estim_specs = tf.estimator.EstimatorSpec(
        mode=mode,
        predictions=pred_classes,
        loss=loss_op,
        train_op=train_op,
        eval_metric_ops={'accuracy': acc_op})

    return estim_specs




# Build the Estimator
model = tf.estimator.Estimator(model_fn)


training_set = tf.contrib.learn.datasets.base.load_csv_without_header(
    filename='C:\\Users\\choppe\\Desktop\\full_trains_withOutFileNames.csv',
    target_dtype=np.int,
    features_dtype=np.float32)

test_set = tf.contrib.learn.datasets.base.load_csv_without_header(
     filename="C:\\Users\\choppe\\Desktop\\testData_withoutFileNames_withLabels.csv",
     target_dtype=np.int,
     features_dtype=np.float32)

# Define the input function for training
input_fn = tf.estimator.inputs.numpy_input_fn(
    x={'sample': training_set[0]}, y=training_set[1],
    # Complete is 9983
    batch_size=9983, num_epochs=None, shuffle=True)
# Train the Model
model.train(input_fn, steps=num_steps)

print("Training done")

# Evaluate the Model
# Define the input function for evaluating
input_fn = tf.estimator.inputs.numpy_input_fn(
     x={'sample': test_set[0]},
     # Complete is 1109
     batch_size=1109, shuffle=False)

# Use the Estimator 'evaluate' method
predictions = list(model.predict(input_fn))
#predicted_classes = [p[0] for p in predictions]
predicted_classes = [p for p in predictions]
print(
    "New Samples, Class Predictions:    {}\n"
        .format(predicted_classes))
